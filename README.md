# Minecraft Server for Linux

[![made-with-bash](https://img.shields.io/badge/Made%20with-Bash-1f425f.svg)](https://www.gnu.org/software/bash/)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/vachmara/minecraft-server-linux/-/blob/a37db4fb165b205e177c679d9fb194069b40f1b5/LICENSE)



Easily install and run a minecraft server on Linux!

## Installing
```` bash
git clone https://github.com/lufinkey/minecraft-server-linux
cd minecraft-server-linux
sudo ./install.sh
````
This will install the minecraft-server service and the minecraft-server script with official minecraft version.
To install Spigot version please run this command instead of the last one:

```` bash
sudo ./install.sh bukkit
````

## Usage
Server files will be stored in ````/var/minecraft/servers/default````.
They aren't created until the first time you start the server.

To start running your minecraft server:
```` bash
sudo service minecraft-server start
````
To stop running your minecraft server:
```` bash
sudo service minecraft-server stop
````

## Advanced usage

Update server to Spigot / Crafbukkit version:
```` bash
sudo ./minecraft-server run your_server_name bukkit
````
If you want change the default server running through minecraft-server service. Change `server_name="your_server_name"` variable available [here](https://github.com/vachmara/minecraft-server-linux/blob/df83419163e16dca5bd86ad7304717ee13700263/minecraft-server.service#L14).
In case you have multiple servers running, don't forget to update `server.properties` port in your minecraft server files.
